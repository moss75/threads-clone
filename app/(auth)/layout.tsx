import {ClerkProvider} from "@clerk/nextjs";
import {Inter} from "next/dist/compiled/@next/font/dist/google";

export const metadata = {
    title: 'Threads',
    description: 'Une application Next.js'
}

const inter = Inter({ subsets: ["latin"] })

export default function RootLayout({
   children
}: {
    children: React.ReactNode
}) {
    return (
        <ClerkProvider>
            <html lang="fr">
                <body className={`${inter.className} bg-dark-1`}>

                </body>
            </html>

        </ClerkProvider>
    )
}